<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Symposium on Zika, Guillian Barre Syndrome and Microcephaly | The UWI Responds to Zika</title>

<link rel="stylesheet" href="../style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/normalize.css" type="text/css" media="screen" />


 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">


<script src="../js/jquery-1.8.2.min.js"></script>
<script src="../js/modernizr-1.7.min.js"></script>


<script src="../js/smooth-scroll.js"></script>
<script src="../js/responsiveImg.js"></script>



<script type="text/javascript" src="../js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="../js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside generic symposium">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
       <div class="sb-toggle-left" id="mobiletoggle">
           	<div class="navlines">
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            </div>
            <div class="navname">Menu</div>
     </div>  
     
     <div id="header">
            <div id="headercontent" class="container_12">
            	<div id="logo" class="push_1 grid_4 alpha">
                	<a href="../index.php"><img src="../images/logo.png" alt="UWI Responds to Zika"/></a>
                </div>
                <div id="mainnav" class="grid_6 push_2 omega">
                	<ul class="nav left grid_3 alpha">
                    	<li id="first"><a href="../index.php">Home</a></li>
                        <li id="second"><a href="../response.php">The UWI's Response</a></li>
                        <li id="third"><a href="../taskforce.php">The Task Force</a></li>
                        <li id="fourth"><a href="../situation.php">Situation Report</a></li>
                    </ul>
                    <ul class="nav right grid_3 omega">
                    	 <li id="fifth" style="display:none;"><a href="coverage.php">Featured Coverage</a></li>
                         <li id="sixth"><a href="../qa.php">Zika Q &amp; A</a></li>
                         <li id="seventh"><a href="../resources.php">Resources for You</a></li>
                         <li id="eighth"><a href="../contactus.php">Contact Us</a></li>
                    </ul>
                </div>
           </div>
      </div>
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_10 omega push_2" id="mainpic">
               		<img src="../images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2 class="grid_5">Symposium on Zika, Guillian Barre Syndrome and Microcephaly</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
       	    <div class="grid_12"><div class="grid_3 alpha omega" id="sidebar">
             	<ul style="visibility:hidden;">
                	<li><a href="#theme">Theme</a></li>
                    <li><a href="#attendance">Who Should Attend</a></li>
<li><a href="#fees">Registration Fees</a></li>
                    <li><a href="#programme">Programme</a></li>
                    <li><a href="#register">Register</a></li>
                </ul>
             </div>
             <a name="theme"></a>
<div class="grid_9 omega" id="copy">
                
                <p>Date: Thursday, March 17, 2016<br>
Time 6.00-8.30 pm<br>
Faculty of Medical Sciences, Teaching and Research Complex, Lecture Theatre 2<br>
University of the West Indies, Mona Campus
</p>
   <p><strong>Organizers:</strong> Ministry of Health, Jamaica; The Pan American Health Organization, and The University of the West Indies<br>
<strong>Chairperson:</strong> Dr. Winston De La Haye, Chief Medical Officer, Ministry of Health
</p>
             
               
<hr />
      
                <h4 style="clear:both;">Thursday, March 17, 2016</h4>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <th width="30%">Time</th>
                    <th width="45%">Item</th>
                  </tr>
                  <tr>
                    <td valign="top" width="30%"><p>6:00-6:20 p.m.</p></td>
                    <td colspan="2" valign="top">Opening<br><br>
Remarks - Dr. Noreen Jack, PAHO/WHO Representative, Jamaica, Bermuda and the Cayman Islands<br><br>

Remarks – Dr. Don Rojas, Consultant to the Vice Chancellor, University of the West Indies<br><br>

Greetings- Honourable Minister, Dr. Christopher Tufton, Minister of Health, Jamaica
</td>
                  </tr>
                  <tr>
                    <td width="30%" valign="top">6:20 p.m.-6:40 p.m.</td>
                    <td colspan="2" valign="top">Zika and arbovirus surveillance, microcephaly and birth defects- recent evidence and implications for health systems <br>
                    <em>Dr Stephane Hugonnet, Medical Officer, Global Preparedness, Surveillance and Response,  WHO </em></td>
                  </tr>
                  <tr>
                    <td width="30%" valign="top">6:40 p.m.-7:00 p.m.</td>
                    <td width="40%" colspan="2" valign="top">Zika and neurological disease-recent evidence and  implications for health systems   <br>
                    <em>Professor Gabriel Cea, Neurologist, Department of Neurological Sciences, University of Chile and PAHO Consultant</em></td>
                  </tr>
                  <tr>
                    <td width="30%" valign="top">7:00 p.m.-7:20 p.m.</td>
                    <td colspan="2"  valign="top">Surveillance of emerging viruses in The Americas: The Role of the Laboratory&quot;.  General concepts and epidemiological background, Laboratory detection surveillance <br>
                    <em>Dr. Juliana Leite, Consultant PAHO/WHO</em></td>
                  </tr>
                  <tr>
                    <td valign="top">7:20 p.m.-7:40 p.m.</td>
                    <td colspan="2"  valign="top">Opportunities for research in Microcephaly- Lessons from HIV-Research <em><br>
                    Professor Celia Christie-Samuels, University of the West Indies</em></td>
                  </tr>
                  <tr>
                    <td valign="top">7:40 p.m.-8:00 p.m.</td>
                    <td colspan="2"  valign="top">ChikV and ZikV Research Technical Working Group in Jamaica <em><br>
                    Dr. Karen Webster, Ministry of Health, Jamaica</em></td>
                  </tr>
                  <tr>
                    <td valign="top">8:00 p.m.-8:25 p.m.</td>
                    <td colspan="2"  valign="top">Panel Discussion-Questions and Answers- <br><em>All presenters</em></td>
                  </tr>
                  <tr>
                    <td valign="top">8:25 p.m.-8:30 p.m.</td>
                    <td colspan="2"  valign="top">Thank you and Closure – <br>                      <em>Dr. Melody Ennis, Ministry of Health, Jamaica</em></td>
                  </tr>
                </table>
<p>&nbsp;</p>
                

                
          </div>
        </div>
        
</div>
  
  </div>
             
</div>   
      
  <?php include("../incl-footer.php"); ?>   