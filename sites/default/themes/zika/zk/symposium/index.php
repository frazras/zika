<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Regional Task Force on Zika Symposium | The UWI Responds to Zika</title>

<link rel="stylesheet" href="../style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/normalize.css" type="text/css" media="screen" />


 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">


<script src="../js/jquery-1.8.2.min.js"></script>
<script src="../js/modernizr-1.7.min.js"></script>


<script src="../js/smooth-scroll.js"></script>
<script src="../js/responsiveImg.js"></script>



<script type="text/javascript" src="../js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="../js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside generic symposium">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
       <div class="sb-toggle-left" id="mobiletoggle">
           	<div class="navlines">
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            </div>
            <div class="navname">Menu</div>
     </div>  
     
     <div id="header">
            <div id="headercontent" class="container_12">
            	<div id="logo" class="push_1 grid_4 alpha">
                	<a href="../index.php"><img src="../images/logo.png" alt="UWI Responds to Zika"/></a>
                </div>
                <div id="mainnav" class="grid_6 push_2 omega">
                	<ul class="nav left grid_3 alpha">
                    	<li id="first"><a href="../index.php">Home</a></li>
                        <li id="second"><a href="../response.php">The UWI's Response</a></li>
                        <li id="third"><a href="../taskforce.php">The Task Force</a></li>
                        <li id="fourth"><a href="../situation.php">Situation Report</a></li>
                    </ul>
                    <ul class="nav right grid_3 omega">
                    	 <li id="fifth" style="display:none;"><a href="coverage.php">Featured Coverage</a></li>
                         <li id="sixth"><a href="../qa.php">Zika Q &amp; A</a></li>
                         <li id="seventh"><a href="../resources.php">Resources for You</a></li>
                         <li id="eighth"><a href="../contactus.php">Contact Us</a></li>
                    </ul>
                </div>
           </div>
      </div>
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_10 omega push_2" id="mainpic">
               		<img src="../images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2 class="grid_5">Regional Task Force<br>on Zika Symposium</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
       	    <div class="grid_12"><div class="grid_3 alpha omega" id="sidebar">
             	<ul>
                	<li><a href="#theme">Theme</a></li>
                    <li><a href="#attendance">Who Should Attend</a></li>
<li><a href="#fees">Registration Fees</a></li>
                    <li><a href="#programme">Programme</a></li>
                    <li><a href="#register">Register</a></li>
                </ul>
             </div>
             <a name="theme"></a>
<div class="grid_9 omega" id="copy">
             	<h3>Theme</h3>
                
                <p>One Environment, One Health, Informing the Caribbean Response to Zika</p>
   
               <div class="row push_1">
               		<img src="../images/inside/coverage/ad_symposium.jpg" />
               </div>
               
               <hr /><a name="attendance"></a>
               <h3>Who Should Attend?</h3>
               <p>The symposium aims to address some of the most critical issues surrounding the Caribbean’s response to Zika. The programme has been specifically designed for the following stakeholder groups:</p>
<ul>
<li>Academia</li>  
<li>Regional ministries (particularly Ministries of Health, Environment and Tourism)</li>  
<li>Professional associations (e.g. tourism, small business association, disaster management etc.) </li>  
    </ul>         
                <hr /><a name="fees"></a>
               <h3>Registration Fees</h3>
                   <ul>
                    <li><strong>One (1) day registration fee</strong> - US$100.00</li>
                    <li><strong>Full symposium fee</strong> - US$200.00</li>
                    <li><strong>Full postgraduate fee</strong> - US$100.00</li>
                   </ul>

                
                <hr />
               <a name="programme"></a> 
      <div class="alpha grid_4"><h3>Programme</h3></div>
      <div class="omega grid_5" style="text-align:right;margin-top:5px;">
      	<a class="pdf" href="../docs/symposium_programme.pdf"><small>Download full programme</small></a>
      </div>
      
                <h4 style="clear:both;">Friday March 4, 2016</h4>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <th width="25%">Time</th>
                    <th width="45%">Item</th>
                    <th width="30%">Remarks</th>
                  </tr>
                  <tr>
                    <td colspan="3" valign="top" align="center"><strong> Hilton   Hotel, Bridgetown, Barbados</strong></td>
                  </tr>
                  <tr>
                    <td width="25%">8:30   a.m. – 9:30 a.m.</td>
                    <td colspan="2">Registration</td>
                  </tr>
                  <tr>
                    <td valign="top" width="25%"><p>9:30   a.m. – 11:00 a.m.</p></td>
                    <td valign="top"><strong>Opening Ceremony (Chair: Clive Landis)</strong><br>
                      Welcome remarks:<br>
                      CARICOM<br>
                        MOH, Barbados<br>
                       The UWI<br>
                    </td>
                    <td valign="top">
                      Speakers  T.B.A.<br>
                    Order subject to protocol</td>
                  </tr>
                  <tr>
                    <td valign="top" width="25%">11:00   a.m. – 11:30 a.m.</td>
                    <td colspan="2" valign="top">Coffee   Break</td>
                  </tr>
                  <tr>
                    <td width="25%" rowspan="4" valign="top">11:30   a.m. – 1:00 p.m.</td>
                    <td colspan="2" valign="top"><strong>History, epidemiology and impact of the Zika epidemic (Chair: James Hospedales)</strong></td>
                  </tr>
                  <tr>
                    <td valign="top">Emerging  viruses in the global perspective</td>
                    <td width="30%" valign="top">Eric van Gorp, Erasmus Medical Centre, Holland</td>
                  </tr>
                  <tr>
                    <td valign="top">Zika   in the Americas</td>
                    <td width="30%" valign="top">Christine Carrington, The UWI St. Augustine, Trinidad</td>
                  </tr>
                  <tr>
                    <td valign="top">CDC   guidelines for pregnant women</td>
                    <td width="30%" valign="top">Rachel Albalak, CDC – US Embassy, Barbados</td>
                  </tr>
                  <tr>
                    <td valign="top" width="25%">1:00   p.m. – 2:00 p.m.</td>
                    <td colspan="2" valign="top">Lunch</td>
                  </tr>
                  <tr>
                    <td width="25%" rowspan="4" valign="top">2:00   p.m. – 3:30 p.m.</td>
                    <td colspan="2" valign="top"><strong><em>Aedes   aegypti</em></strong><strong> biology and   control (Chair: John Lindo)</strong><br></td>
                  </tr>
                  <tr>
                    <td valign="top"><em>Aedes</em> biology and   adaptation </td>
                    <td width="30%" valign="top">Dave Chadee,   The UWI St. Augustine, Trinidad</td>
                  </tr>
                  <tr>
                    <td valign="top">Mosquito   prevention approaches: field results from Brazil </td>
                    <td width="40%" valign="top">Dave Chadee,   The UWI St. Augustine, Trinidad</td>
                  </tr>
                  <tr>
                    <td valign="top">The   ‘one health’ concept for the control of spread of zoonotic diseases</td>
                    <td width="30%" valign="top">Abiodun Adesiyun,   The UWI St. Augustine, Trinidad</td>
                  </tr>
                  <tr>
                    <td valign="top" width="30%">3:30   p.m. – 4:00 p.m.</td>
                    <td colspan="2" valign="top">Afternoon   Break</td>
                  </tr>
                  <tr>
                    <td width="30%" rowspan="4" valign="top">4:00   p.m. – 5:30 p.m.</td>
                    <td colspan="2"  valign="top"><strong>Environment and the Economy (Chair:   Dave Chadee)</strong><br></td>
                  </tr>
                  <tr>
                    <td  valign="top">Water   quality in three Caribbean sites</td>
                    <td valign="top"> Adrian   Cashman, The UWI Cave Hill, Barbados</td>
                  </tr>
                  <tr>
                    <td  valign="top">Environmental   quality: impact on tourism </td>
                    <td valign="top">Janice   Cumberbatch, The UWI Cave Hill, Barbados</td>
                  </tr>
                  <tr>
                    <td  valign="top">Impact   of Zika on Caribbean economies</td>
                    <td valign="top">Winston   Moore, The UWI Cave Hill, Barbados </td>
                  </tr>
                </table>
<p>&nbsp;</p>
                
                
              <h4>Saturday March 5, 2016</h4>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <th width="25%">Time</th>
                    <th width="45%">Item</th>
                    <th width="30%">Remarks</th>
                  </tr>
                  <tr>
                    <td colspan="3" valign="top" align="center"><strong>Hilton Hotel, Bridgetown,   Barbados</strong></td>
                  </tr>
                  <tr>
                    <td width="25%" rowspan="4" valign="top">09:00 a.m. –   10:30 a.m.</td>
                    <td colspan="2" valign="top"><strong>Case definitions and complications&nbsp; (Chair: Paul Teelucksingh)</strong><br></td>
                  </tr>
                  <tr>
                    <td valign="top">Adult   neurological syndromes: experience in Suriname </td>
                    <td width="30%" valign="top">Eric van Gorp, Erasmus MC, Holland</td>
                  </tr>
                  <tr>
                    <td valign="top">WHO case   definitions:<br>
discussion/adaptation </td>
                    <td width="30%" valign="top">Jean Marie Rwangabwoba, PAHO, Barbados </td>
                  </tr>
                  <tr>
                    <td valign="top">Surveillance   and reporting</td>
                    <td valign="top">James Hospedales, CARPHA, Trinidad</td>
                  </tr>
                  <tr>
                    <td valign="top">10:30 a.m. –   11:00 a.m.</td>
                    <td colspan="2" valign="top">Coffee Break</td>
                  </tr>
                  <tr>
                    <td rowspan="4" valign="top">11:00 a.m. –   12:30 p.m.</td>
                    <td colspan="2" valign="top"><strong>Engagement at country level for Zika preparedness &nbsp;(Chair: Angela Rose)</strong><br></td>
                  </tr>
                  <tr>
                    <td valign="top">Preparing for   outbreaks – lessons from ebola</td>
                    <td valign="top">Angela Rose, The UWI Cave Hill </td>
                  </tr>
                  <tr>
                    <td valign="top">Zika prevention   approaches in Jamaica</td>
                    <td valign="top">John Lindo, The UWI Mona</td>
                  </tr>
                  <tr>
                    <td valign="top">US Congressional   assistance for country Zika plans </td>
                    <td valign="top">Tiaji Salaam-Blyther, Library of Congress, Washington DC</td>
                  </tr>
                  <tr>
                    <td valign="top">12:30 p.m. –   1:30 p.m.</td>
                    <td colspan="2" valign="top">Lunch </td>
                  </tr>
                  <tr>
                    <td rowspan="4" valign="top">1:30 p.m. – 3:00   p.m.</td>
                    <td colspan="2" valign="top"><strong>Communication and continuing education (Chair: Winston Moore)</strong><br></td>
                  </tr>
                  <tr>
                    <td valign="top">The UWI Zika hub</td>
                    <td valign="top">Dawn-Marie De Four-Gill, The UWI Regional Headquarters</td>
                  </tr>
                  <tr>
                    <td valign="top">Continuing public   education </td>
                    <td valign="top">Veronica Simon, The UWI Open Campus, St. Lucia</td>
                  </tr>
                  <tr>
                    <td valign="top">Implications for pediatric healthcare: lessons learned from HIV</td>
                    <td valign="top">Celia Christie-Samuels, The UWI Mona Jamaica</td>
                  </tr>
                  <tr>
                    <td valign="top">3:00 p.m. –   3:30 p.m.</td>
                    <td colspan="2" valign="top">Afternoon Break</td>
                  </tr>
                  <tr>
                    <td valign="top">3:30 p.m. – 5:00   p.m.</td>
                    <td valign="top"><strong>Deploying The UWI Academy against Zika &nbsp;(Chair: Glenford Howe)</strong><br>
                      Round table   discussion on making available The UWI resources available to serve the region </td>
                    <td valign="top">
                      Facilitator: Glenford Howe, The UWI Open Campus</td>
                  </tr>
                  <tr>
                    <td valign="top">5:00 p.m. – 5:30   p.m.</td>
                    <td valign="top"><strong>Recap and Wrap Up</strong></td>
                    <td valign="top">Dale Webber, The UWI Regional Headquarters</td>
                  </tr>
                </table>
&nbsp;
                
                
                
                
<hr /><a name="register"></a>
                <h3>Register</h3>
                <p>Please complete the form to begin the registration process. Registrants must complete and return this form by  either email or fax.</p>
                <a class="pdf" href="../docs/form_register.pdf"><strong>Download Registration Form</strong></a>
                
          </div>
        </div>
        
</div>
  
  </div>
             
</div>   
      
  <?php include("../incl-footer.php"); ?>   