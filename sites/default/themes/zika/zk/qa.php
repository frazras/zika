<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Zika Q &amp; A |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />


 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside qa">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/qa/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Zika Q &amp; A</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	<div class="grid_12"> <div class="grid_3 alpha" id="sidebar">
             	<ul>
                	<li><a href="#whatis">What is the Zika Virus?</a></li>
                    <li><a href="#spread">How is Zika spread?</a></li>
                    <li><a href="#risk">Who is at Risk?</a></li>
                    <li><a href="#symptoms">Know the Symptoms</a></li>
                    <li><a href="#prevention">Zika Prevention</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="whatis"></a>
                 <h3>What is the Zika Virus?</h3>
                 <div class="grid_3 alpha omega">
                 	<h1>1952</h1>
                 </div>
                 <div class="grid_6 row">
                 	<img src="images/inside/qa/pic_uganda.jpg" />
                    <p>The Zika virus (ZIKV) is an emerging mosquito-borne virus that was first identified in humans in 1952 in Uganda and the United Republic of Tanzania. Outbreaks of Zika have been recorded in Africa, Asia, the Pacific and most recently in the Americas.</p>
                 </div> 
                 
                 <div class="grid_3 alpha omega">
                 	<h1>2016</h1>
                 </div>
                 <div class="grid_6 row">
                 	<img src="images/inside/qa/pic_microcephaly.jpg" />
                    <p>On February 01, 2016 the World Health Organisation announced that clusters of microcephaly (responsible for abnormal smallness of the head associated with incomplete brain development) and other neurological disorders potentially linked to the spread of the Zika virus represents a Public Health Emergency of International Concern (PHEIC).</p>
                 </div> 
                 <div style="margin-top:0px;margin-bottom:20px;text-align:center;float:left;width:100%;">
                        <iframe style="width:100%;min-height:390px !important;height:auto;" src="https://www.youtube.com/embed/aqGc-SVyG_Y" frameborder="0" allowfullscreen></iframe>
                   </div>             
                <hr />
                <a name="spread"></a>
                <h3>How is Zika spread?</h3>
                <p>The <em>Aedes aegypti</em> is the principal mosquito species that transmits the Zika virus, Dengue, Chickungunya and Yellow Fever. </p>

<h4>Mother to Child</h4> 
<p>The Zika virus can be transferred from mother to child during pregnancy. </p> 

<h4>Sexual Transmission</h4> 
<p>There is also evidence that the virus can be transmitted from a man to his sex partners. In the two recorded instances of likely sexual transmission, both men displayed symptoms of the virus. It is not yet known if a woman can transfer the virus to her sex partners (CDC). For more on the Zika Virus and sexual transmission visit the Centers for Disease Control and Prevention.</p> 
 					<div style="margin-top:20px;margin-bottom:20px;text-align:center;float:left;width:100%;">
                        <iframe style="width:100%;min-height:390px !important;height:auto;" src="https://www.youtube.com/embed/v-PVpYDVbLw" frameborder="0" allowfullscreen></iframe>
                   </div>   
<hr /><a name="risk"></a> 
               <h3>Who is at Risk?</h3>
                <p>Persons living in territories where the Aedes aegypti mosquito species is present are at risk. </p>
 
<p>High risk persons include infants and elderly who are unable to protect themselves against mosquito bites.</p> 

<p>Given the suspected association between the Zika virus and birth defects pregnant women are especially advised to take precaution against contracting the virus. The unborn are listed as the most at risk group.
</p>

			<hr /><a name="symptoms"></a> 
            	<h3>Know the Symptoms</h3>
                <p>Infection with Zika virus may be suspected based on symptoms and history including travel to a region where Zika virus is known to be present. The virus however can only be confirmed by laboratory testing.</p>
                
                <p>The incubation period (time from exposure to symptoms) for the Zika virus is likely to be a few days. One in four persons who contract the virus will exhibit symptoms. These can include:</p>
                <ul>
                	<li>Mild fever</li>
                	<li>Conjunctivitis (reddening of the eyes)</li>
                    <li>Muscle and joint pain</li>
                    <li>Skin rashes</li>
                </ul>
				<p>The symptoms are usually mild and normally last for 2-7 days. There is no specific treatment or vaccine currently available. Persons exhibiting symptoms should get plenty of rest, drink sufficient fluids and may treat pain and fever with common medicine. </p>

If symptoms persist individuals should seek treatment from a physician. 
</p>
<hr /><a name="prevention"></a> 
			<h3>Zika Prevention</h3>
            <p>Prevention and control of the Zika virus relies on the reduction of mosquito populations through source reduction (the removal and modification of breeding sites) and reduced contact between mosquitos and people.</p>

<p>Persons can reduce the risk of being bitten by the use of</p> 
<ul>
<li>insect repellents</li>
<li>light coloured protective clothing</li>
<li>physical barriers such as screens, closed doors and windows and mosquito nets for sleeping.</li>
</ul>

<p>Critical to prevention is the elimination of mosquito breeding sites in both public and private places. Public Health authorities have increased insecticide spraying/fogging activities and citizens are advised to inspect their homes and environs and take action to eliminate standing water. It is also advised that containers which housed potential breeding site be carefully scrubbed to destroy any mosquito eggs adhering to the walls of the container.</p> 
<p>To prevent the possible sexual transfer of the virus, persons who have traveled to or live in territories where the Zika virus is present can abstain or correctly use a condom in every instance of intercourse. For more on the Zika Virus and sexual transmission visit the <a href="http://www.cdc.gov/zika/transmission/sexual-transmission.html" target="_blank">Centers for Disease Control and Prevention</a>.</p>
				<div style="margin-top:20px;margin-bottom:20px;text-align:center;float:left;width:100%;">
                        <iframe style="width:100%;min-height:390px !important;height:auto;" src="https://www.youtube.com/embed/xrNQ3pmgeC8" frameborder="0" allowfullscreen></iframe>
                   </div> 
            
        </div>    

          </div>      
             </div>
        </div>
        
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   