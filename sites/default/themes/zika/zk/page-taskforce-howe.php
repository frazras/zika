<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dr. Glenford Howe</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Dr. Glenford Howe</h3>
            <p>Dr Glenford D. Howe is the Senior Programme/Research Officer (Professorial level) in the Planning and Institutional Research Unit of The University of the West Indies’ (UWI), Open Campus.</p> 
<p>Dr Howe has published extensively in various disciplines including, History, Politics, Governance, Education, Health and Child Protection. His doctoral thesis (University of London) titled <em>Race War and Nationalism: A Social History of West Indians in the First World War</em> was turned into a documentary by Channel 4 TV in the UK and published as a book. He also produced together with Professor Alan Cobley the first book on HIV/AIDS in the Caribbean, and was also joint organizer of an international conference on the <em>History of Medicine and issues in present day public health in the Caribbean</em> held in Barbados, 2001.  In addition to his new role in the regional Zika Task Force, Dr Howe has been commissioned as an expert by numerous regional and international bodies including UNESCO, UNICEF, UNDP, CDB, BBC, CARICOM, the Office of the Vice Chancellor, The UWI and the governments of Barbados, Belize and Anguilla.</p> 
<p>Dr Howe has served as Associate Editor (Caribbean co-editor) of the <em>Inter-American Journal of Education for Democracy (IJED)</em> and as a member of the Advisory Board for the Inter-American Programme on Education for Democratic Values and Practices.  He prepared the draft of the CARICOM High Level/ Ministerial declaration:  <em>'The Bridgetown Declaration and Agenda for Action to combat Child Sexual Abuse in the Caribbean'</em>. </p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>