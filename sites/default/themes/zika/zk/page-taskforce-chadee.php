<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Prof. Dave Chadee</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Prof. Dave Chadee</h3>
            <p>Prior to joining The University of the West Indies (UWI), Professor Dave D. Chadee was very involved in the Public Health sector of Trinidad and Tobago holding the posts of Entomologist at the Insect Vector Control Division; Director of the Trinidad Public Health Laboratory; and Deputy Director of Laboratory Services at the Ministry of Health.</p> 

<p>He is currently Professor of Environmental Health and Subject Leader in Bioethics in the Department of Life Sciences at The UWI, St. Augustine. </p> 

<p>A UWI expert most suited to serve on the regional Zika Task Force, Professor Chadee’s area of specialty includes the ecology, surveillance, ethics, epidemiology and control of vector-borne diseases. He has published extensively; over 300 publications in international journals, and has numerous collaborations with scientists from the USA and the UK.  He runs the only Postgraduate course in Bioethics for research students at the UWI St Augustine Campus, and serves on the UWI Ethics Committee.</p> 

<p>Professor Chadee currently holds an Adjunct Professor post at the Department of Tropical Medicine, School of Public Health and Tropical Medicine, Tulane University, New Orleans, USA, an Adjunct Professor position in the Department of Epidemiology and Public Health, University of Miami, Florida, USA and an Adjunct Professor in Public Health at the University of South Florida, Tampa, Florida, USA.
 
</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>