<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Situation Report |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />


 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside situation">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/situation/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Situation Report</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_12"><div class="grid_3 alpha omega" id="sidebar">
             	<ul>
                	<li><a href="#overview">Overview</a></li>
                    <li><a href="#update">Epidemiological Update</a></li>
                    <li><a href="#response">Response</a></li>
                </ul>
             </div>
             
          <div class="grid_9 omega" id="copy"><a name="overview"></a>
             	<h3>Overview</h3>
                
                <p>The University of the West Indies’ (UWI) regional Zika virus Task Force will coordinate its response to the virus threat. The University has always owned its responsibility to provide the region with expert advice and consultative support; in keeping with this, the Task Force will consult on the development of a strategic, scientific approach to tackling the Zika virus. </p>
   
               <div class="row push_1" id="timeline">
               		<img src="images/inside/situation/pic_timeline.png" />
               </div>
               
               <a name="update"></a>
      <h3>Epidemiological Update</h3>
               <p>The broad distribution of the vector (Aedes aegypti) in the Americas combined with the high mobility of persons in and outside of the region represent a risk for the spread of the virus. </p>
<div class="row">
<a href="http://ais.paho.org/phip/viz/ed_zika_countrymap.asp" target="_blank"><img src="images/inside/situation/pic_map.png" /></a>
</div>
<p>Also keep track of the CARICOM states which appear on <a href="http://www.paho.org/hq/index.php?option=com_content&view=article&id=11603&Itemid=41696&lang=en" target="_blank">the PAHO report</a> of countries with autochthonous transmission by epidemiological week (EW).</p>
            
<table width="100%" class="epupdate" style="display:none;">
                  <tr>
                    <th>CARICOM State</th>
                    <th>Epidemiological Week (EW)</th>
                    <th>Calendar Date</th>
                  </tr>
                  <tr>
                    <td>Suriname</td>
                    <td>EW 44 (2015)</td>
                    <td>Nov 1- 7, 2015</td>
                  </tr>
                  <tr>
                    <td>Haiti</td>
                    <td>EW 1 (2016)</td>
                    <td>Jan 3 – 9, 2016</td>
                  </tr>
                  <tr>
                    <td>Barbados</td>
                    <td>EW 2 (2016)</td>
                    <td>Jan 10 – 16, 2016</td>
                  </tr>
                  <tr>
                    <td>Guyana</td>
                    <td>EW 2 (2016)</td>
                    <td>Jan 10 – 16, 2016</td>
                  </tr>
                  <tr>
                    <td>Jamaica</td>
                    <td>EW 4 (2016)</td>
                    <td>Jan 24 – 30, 2016 </td>
                  </tr>
                </table>
                
                <h4>Neurological syndrome, congenital anomalies, and Zika virus infection</h4>
                <p>Six countries including one CARICOM member state (Brazil, FrenchPolynesia, El Salvador, Venezuela, Colombia and Suriname) have reported increases in the incidence of cases of microcephaly and/or Guillain-Barre syndrome (GBS) in conjunction with an outbreak of Zika.</p>
                
                <blockquote>
                	Preliminary findings suggest that ZIKV infection during pregnancy appears to be associated with serious outcomes, including fetal death, placental insufficiency, fetal growth restriction, and central nervous system (CNS) injury. 
                </blockquote>
                <p class="blockquote-author">PAHO</p>
                
                <hr />
                <a name="response"></a>
    <h3>Response</h3>
                <h4>Partner Coordination &amp; State Responses </h4>
                <h5>OECS</h5>
                <p>Numerous Caribbean states have announced states of emergency over the Zika virus outbreak.</p>
<p>Acknowledging the Organisation of Eastern Caribbean States (OECS) to be a single health and economic space the OECS Ministers’ of Health have agreed on a harmonized approach to Zika. The OECS-wide campaign will include:
</p>
                <ul>
                	<li><h5>Monitoring and Surveillance</h5>
                    	<p>A close alliance of national, regional and international public health agencies to monitor the disease and attempt to anticipate trends.</p></li>
                     <li><h5>Eradication and Protection Action</h5>
                    	<p>Activity by national authorities, communities and work places aimed at the rapid elimination of the mosquito population.</p></li>
                    <li><h5>Care and Case Management</h5>
                    	<p>Equipping public health services to manage cases which may occur.</p></li>
                    <li><h5>Public Education</h5>
                    	<p>A wide-spread public awareness campaign to inform and educate on preventatives measures and the status of the threat.  </p></li>   
             
                </ul>
                
                <h4>The University of the West Indies</h4>
                <p>Vice-Chancellor of The University of the West Indies, Professor Sir Hilary Beckles on February 11, 2016 announced the formation of a regional Zika virus Task Force intended to leverage the University’s expertise and coordinate efforts with the regional governments and other bodies.</p> 

<p>The Pan American Health Organisation (PAHO) and the Caribbean and Caribbean Public Health Agency (CARPHA) are key advisors on the region’s collaborative efforts. </p> 


<em>Visit the websites of <a href="http://www.who.int/en/" target="_blank">WHO</a>, <a href="http://www.paho.org/hq/" target="_blank">PAHO</a> and <a href="http://carpha.org" target="_blank">CARPHA</a> for frequent situation reports from international and regional perspectives. </em>
                
            </div>
        </div>
     </div>   
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   