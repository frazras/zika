<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dr. Veronica Simon</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Dr. Veronica Simon</h3>
            <p>Dr Veronica Simon has been an educator for 36 years. She is the Head of The University of the West Indies’ (UWI) Open Campus (OC) site in Saint Lucia and is responsible for all its on-island activities. These include public service, outreach, research and continuing education programming. As part of her public education and outreach mandate, Dr Simon has instituted a series of public lectures/discussion. These events are designed to inform, enlighten and stimulate public discourse on a wide range of topics. This expertise is particularly valuable in her new role as a member of the team which will coordinate The UWI’s response to the Zika virus. </p>
<p>Dr Simon currently sits on the Open Campus Academic Board and the University Senate and has served on the Open Campus Council and the Academic Quality Assurance Committee.</p>
<p>Her regional service has included her role as a consultant to the OECS, The UWI, collaborative projects of CARICOM/OAS/CIDA and CXC.</p>  
<p>A curriculum specialist, Dr Simon has designed learning materials for all age groups from primary to tertiary level and focuses on enabling change by way of formal documentation of knowledge as well as through the creation of open, discursive environments. In Saint Lucia, she is the Chair of the Global Environment Facility Small Grants Programme’s (GEF/SGP) National Steering Committee; Secretary of the National Heroes Commission; Member of the Sir Arthur Lewis Community College Academic Board and the Saint Lucia Medical Schools Monitoring and Accreditation Committee. 
</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>