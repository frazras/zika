<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<meta name="Description" content="Join our Live Stream" />
<meta name="Keywords" content="zika,Caribbean,zika virus,University of the West Indies,UWI,University of the West Indies Zika Task Force,live stream" />
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META NAME="ROBOTS" CONTENT="INDEX">

<title>Livestream |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="../style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/1044_12_10_10.css" type="text/css" media="screen" />

 
 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">


<script src="../js/jquery-1.8.2.min.js"></script>
<script src="../js/modernizr-1.7.min.js"></script>


<script src="../js/smooth-scroll.js"></script>
<script src="../js/responsiveImg.js"></script>




<body class="inside generic stream">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
     <div class="sb-toggle-left" id="mobiletoggle">
           	<div class="navlines">
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            </div>
            <div class="navname">Menu</div>
     </div>  
     
     <div id="header">
            <div id="headercontent" class="container_12">
            	<div id="logo" class="push_1 grid_4 alpha">
                	<a href="../index.php"><img src="../images/logo.png" alt="UWI Responds to Zika"/></a>
                </div>
                <div id="mainnav" class="grid_6 push_2 omega">
                	<ul class="nav left grid_3 alpha">
                    	<li id="first"><a href="../index.php">Home</a></li>
                        <li id="second"><a href="../response.php">The UWI's Response</a></li>
                        <li id="third"><a href="../taskforce.php">The Task Force</a></li>
                        <li id="fourth"><a href="../situation.php">Situation Report</a></li>
                    </ul>
                    <ul class="nav right grid_3 omega">
                    	 <li id="fifth" style="display:none;"><a href="coverage.php">Featured Coverage</a></li>
                         <li id="sixth"><a href="../qa.php">Zika Q &amp; A</a></li>
                         <li id="seventh"><a href="../resources.php">Resources for You</a></li>
                         <li id="eighth"><a href="../contactus.php">Contact Us</a></li>
                    </ul>
                </div>
           </div>
      </div>
       
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_10 omega push_2" id="mainpic">
               		<img src="../images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Livestream</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
       	    <div class="grid_12"><div class="grid_3 alpha" id="sidebar">
             	<ul style="visibility:hidden;">
                	<li><a href="#news">News</a></li>
                    <li><a href="#events">Events</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="news"></a>
                  <h4>Live Stream: UWI Responds to Zika Press Conference</h4>
<p><strong>Vice-Chancellor Professor Sir Hilary Beckles outlines the mandate and work of the UWI Regional Task Force on Zika</strong><br>
Thursday 10, March 2016<br>
11:00 am AST</p>

<h5>Instructions to participate in the press conference Q &amp; A session:</h5>
<p>Call <strong>(246) 417-4800</strong> and you will be requested to enter a 4 digit meeting ID 7002<br>
Or Skype <strong>cave.hill</strong></p>

<div class="streaming">
<script src="//content.jwplatform.com/players/L1cHPMm3-qMLxFQBv.js"></script>
</div>
<small>Due to heavy internet traffic, our live stream connection may pause intermittently. Please refresh your browser window to continue viewing.</small>

        <div class="outerdiv row" style="display:none;">
        	<iframe id="innerdiv" src="http://www.cavehill.uwi.edu/videos/livestream.asp" style="border:0px #ffffff none;" name="myiFrame" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="480px" width="854px"></iframe>
        </div>  
          <div class="chatting">
           <script id="cid0020000092088348109" data-cfasync="false" async src="//st.chatango.com/js/gz/emb.js" style="width:680px;height: 400px;">{"handle":"cavehill-wide","arch":"js","styles":{"a":"48ab4d","b":100,"c":"000000","d":"000000","k":"48ab4d","l":"48ab4d","m":"48ab4d","p":"10","q":"48ab4d","r":100,"surl":0,"cnrs":"0.35","fwtickm":1}}</script>

</script>            
                
          </div>      
             </div>
        </div>
        
</div>
  
 </div>
      
  <?php include("../incl-footer.php"); ?>   