<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dr. Winston Moore</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Dr. Winston Moore</h3>
            <p>Dr Winston Moore is presently Professor of Economics and Head of the Department of Economics at The University of the West Indies (UWI), Cave Hill.  He brings to the regional Zika Task Force the skill required to deliver on its mandate to conduct economic impact studies related to the virus outbreak.</p> 
 <p>Prior to his appointment at The UWI, Dr Moore held the position of Senior Economist at the Central Bank of Barbados.  His recent research has examined the issues surrounding the Green Economy, Private Sector Development as well as the Economic Impact of Climate Change on Tourism. </p> 
 <p>Dr Moore has published more than 80 peer-reviewed papers, books and book chapters.  He holds a PhD in Economics from the University of Surrey; an MSc in Economics from the University of Warwick; and a BSc in Economics from The University of West Indies, Cave Hill. 
</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>