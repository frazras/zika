<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Prof. Dale Webber</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Prof. Dale Webber</h3>
            <p>Professor Dale Webber is currently Pro-Vice Chancellor with the responsibility for Graduate Studies and Research throughout The University of the West Indies (UWI) and presides over the Board for Graduate Studies and Research.</p> 

<p>Professor Webber, having attained PhD in Marine Botany from The University of the West Indies in 1990, moved on to a distinguished career in research, teaching and administration. For the past five years he has been the holder of The UWI James Seivright Moss-Solomon Snr. Chair of Environmental Management, a Professorship endowed by the Grace Kennedy Foundation. A Coastal Ecologist and environmentalist at heart, he sits on several private and public sector boards where he is actively involved in the initiatives and projects geared at the identification and analysis of hazards to the West Indian Coastal and Marine Environments with a view towards its preservation and development.  His renowned research in Coastal Ecology and Environmental Management, specifically regarding the Kingston Harbour and the Pedro Cays, has impacted both Jamaican and international policy. </p>   

<p>Professor Webber has authored five book chapters and thirty-five publications in peer reviewed journals. His membership in the regional Task Force aligns with his quest to contribute to the environmental, economic and social development of the Caribbean region.  

</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>