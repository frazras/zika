<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Prof. Eric van Gorp</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Prof. Eric van Gorp</h3>
            <p>Professor Dr Eric C.M. van Gorp is an Infectious Disease Specialist with significant international experience. </p> 

<p>He is currently working at the Erasmus MC Rotterdam, the Netherlands, at the Department of Internal Medicine and Infectious Diseases and at the Department of Viroscience. He is visiting Professor at the University of Airlanga in Surabaya, Indonesia and also involved in the Rotterdam Global Health Initiative.  His field of interest is the social and scientific impact of infectious diseases; epidemiology, clinical management and pathophysiology. The research line, with several PhDs which he is supervising, is a focus on exotic viral infections, among which are the hemorrhagic fevers, HIV and Fever studies, with an extensive (inter) national network in the Netherlands, the Caribbean, Suriname, China and Indonesia. </p> 

<p>Professor van Gorp’s own studies started with pursing qualifications in the medical field at the University of Amsterdam. In 1990 he graduated as a medical doctor, after which he started to work as a resident in the Department of Internal Medicine of the Academic Medical Centre (AMC) Amsterdam. In 1997 he graduated as specialist in Internal Medicine and continued his training as an Infectious Disease Specialist graduating in 1998. In 2001 he finished his thesis entitled: "Studies on the pathophysiology of dengue haemorrhagic fever and dengue shock syndrome", granted by the Royal Netherlands Academy of Arts and Sciences (KNAW).</p>  

<p>Professor van Gorp is Supervisor of the Vaccination Outpatient Clinic and the Cinical Trial Unit from the Viroscience Department.


</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>