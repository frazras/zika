<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<meta name="Description" content="The University of the West Indies (UWI) has convened a regional Zika Task Force to coordinate the Caribbean’s response to the Zika threat and lend expert advice and consultative support to the region. As members of The UWI community and its partners respond to the outbreak, this website provides updates and helpful resources on Zika in the Caribbean." />
<meta name="Keywords" content="zika,Caribbean,zika virus,University of the West Indies,UWI,University of the West Indies Zika Task Force" />

<title>Home |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />

 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">



<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Cookie/Welcome function -->

<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript">
  var jq122 = jQuery.noConflict();
jq99(document).ready(function($) {
    if (jq122.cookie('noShowWelcome')) jq122('.welcome2').hide();
    else {
        $("#close-welcome").click(function() {
            $(".welcome").fadeOut(500);
            $.cookie('noShowWelcome', true);    
        });
    }
});
</script> 

<!-- bxSlider -->
<script type="text/javascript" src="js/jquery.bxslider.js"></script>

<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 815,
    slideMargin: 20,
    pause: 7000,
    auto:  true,
    pager: true,
    controls: false
  });
});
</script>


<body class="home">

<div class="outer" id="sb-site">
	<div id="wrap">
    
  
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="homecontent">
          
        <a name="top"></a>
        <div id="midarea" class="container_12">
        	 
            <div class="container_12" id="slides">
            <div class="grid_12 alpha">
                <div id="news-featured" class="grid_2 alpha">
                    <small class="grid_2 alpha"><strong>29 Feb 2016</strong></small>
                    <a href="http://www.thelancet.com/journals/lancet/article/PIIS0140-6736%2816%2900562-6/fulltext" target="_blank"><p>Guillain-Barré Syndrome outbreak associated with Zika virus infection in French Polynesia: a case-control study</p></a>
                    <small>Source: Lancet</small>
                </div>
                <div id="slideshow" class="grid_10 omega">
                		<!--ul id="slidebx">
                          <li--><a href="symposium.php"><img src="images/home/slider_2.jpg" /></a><br><!--/li-->
                			<!--li><a href="docs/UWI_ZikaInfographic_05.pdf"><img src="images/home/slider_3.jpg" /></a></li>
                    	</ul-->
                </div>
              </div>
            </div>
            <div class="container_12">
            	<div class="grid_2 alpha">
                    <a href="taskforce.php">
                        <div id="about" class="box-green grid_2 alpha">
                            <h4>About the<br>Task Force</h4>
                        </div>
                   </a>
                   <div id="ask-expert" class="alpha">
                        <a href="askexpert.php"><img src="images/home/pic_home_askexpert.jpg" /></a>
                   </div>
            	</div>
                <div class="grid_5 omega" id="whatszika">
                    <a href="qa.php"><img src="images/home/pic_home_whatiszika.jpg" /></a>
                </div>
                <a class="fancybox-media" href="https://www.youtube.com/watch?v=iOm15VyWlwo">
                    <div class="grid_2 alpha" id="whoqa">
                        <img src="images/home/pic_home_WHOQA.jpg" />
                    </div>
                </a>
           </div>
        <div class="container_12">
          <div class="grid_12 alpha omega">
          	<div class="grid_7 alpha omega" id="hashtag">
        		<img src="images/home/hashtag.png" />
        	</div>
            <div class="grid_5 omega alpha" id="prevention">
                <a href="qa.php#prevention">
                    <img src="images/home/pic_home_zikaspread.jpg" />
                </a>
            </div>
          </div>
       </div>
       <a href="situation.php">
       		<div class="box-purple grid_5 alpha omega itema" id="spread">
				<h3>Zika's Regional<br />Spread</h3>
			</div>
        </a>
        <div class="grid_2 alpha itema omega" id="cases">
        	<img src="images/home/pic_cases.png" />
        </div>
        <div class="grid_5 omega itema" id="states">
        	<img src="images/home/pic_states.png" />
        </div>
	</div>
  
  <div id="bottomarea" class="container_12">
  	<div class="grid_12 alpha omega">
        <div class="grid_2 box-teal row alpha omega item" id="infobox">
            <h3>Resources For</h3>
        </div>
        <a href="resources_healthcare.php">
            <div class="grid_2 alpha item omega">
                <img src="images/home/pic_home_healthcare.jpg" />
            </div>
        </a>
          <a href="resources_policymakers.php">
            <div class="grid_2 alpha item omega">
                <img src="images/home/pic_home_policy.jpg" />
          </div>
        </a>
          <a href="resources_public.php">
            <div class="grid_2 alpha item omega">
                <img src="images/home/pic_home_public.jpg" />
            </div>
        </a>
    </div>
    <div class="container_12" id="mediacentre">
      <div class="grid_12">
    	<div class="grid_2 alpha omega">
        	<h3>Media Centre</h3>
        </div>
        
        <div class="grid_8 alpha push_1 omega">
        	<div class="responsive-tabs">
              <h4>News &amp; Updates</h4>
              		<div class="content">
                        <ul>
                            <li><a href="http://www.who.int/entity/emergencies/zika-virus/situation-report-26-02-2016.pdf?ua=1" target="_blank">Zika virus, Microcephaly, and Guillain–Barré syndrome situation report</a><br ><small>26 Feb 2016  |  WHO</small></li>
                            <li><a href="http://www.who.int/entity/emergencies/zika-virus/situation-report/19-february-2016/en/index.html" target="_blank">Zika, Microcephaly, and Guillain–Barré syndrome situation report</a><br ><small>8 Feb 2016  |  WHO</small></li>
                            <li><a href="http://www.who.int/entity/emergencies/zika-virus/situation-report/12-february-2016/en/index.html" target="_blank">Situation report: Zika and potential complications</a><br ><small>5 Feb 2016  |  WHO</small></li>
                        </ul>
                      </div>
              <h4>Events</h4>
              <div class="content">
                  <p><strong><a href="symposium.php">One Environment, One Health, Informing the Caribbean Response to Zika</a></strong><br>
                  Regional Task Force on Zika Symposium</p>
                  <small>4-5 March, 2016</small>
             </div>
             <h4>Media Contacts</h4>
             <div class="content">
             <p><strong>University Marketing and Communications Office</strong><br>
The University of the West Indies<br>
St. Augustine Campus<br>
TRINIDAD</p>

<p><strong>E:</strong> <a href="mailto:marketing.communications@sta.uwi.edu">marketing.communications@sta.uwi.edu</a><br> 
<strong>T:</strong> 868-662-2002 exts 82316 / 84246 <br>
<strong>F:</strong> 868-662-3858</p>
             </div>
        </div>
      </div>
     </div> 
      
        </div>
    </div>
</div>
  
 <!-- 
 <div class="welcome box-green">
	<div class="container_12">
    	<div class="grid_12">
			 <div class="grid_5 omega alpha" id="blurb-vc">
                    <img src="images/home/bg_home_blockquote.png" /> 
             </div>
             <div class="grid_6 push_2 omega alpha">
                    <h4>About this website</h4>
                    <p>On February 01, 2016 the World Health Organisation declared that complications potentially associated with the Zika virus represented a Public Health Emergency of International Concern (PHEIC). The current Zika virus outbreak is concentrated in the Americas and the Caribbean and The University of the West Indies (UWI) has convened a regional Zika Task Force to coordinate its response to the threat and lend expert advice and consultative support to the region.</p>
    <p>As members of The UWI community and its partners respond to the outbreak, this website will provide updates and helpful resources on Zika in the Caribbean.</p>
              <div id="close-welcome">
    	<a href="#">Explore website</a>
    </div>
              
              </div>
            
       </div>
       
    </div>
    
</div> -->           
</div>   


      
  <?php include("incl-footer.php"); ?>   