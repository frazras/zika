<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>The UWI's Response | The Task Force</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />


 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside taskforce">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/taskforce/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>The Task Force</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_12"><div class="grid_3 alpha" id="sidebar">
             	<ul style="visibility:hidden;">
                	<li><a href="#context">Context</a></li>
                    <li><a href="#contributions">Contributions</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="context"></a>
                 <p>The University of the West Indies has research expertise and capacity in areas of vector control, clinical research, laboratory assay development, economic impact analysis, education, grant resource mobilisation, patient database management, and communication. The UWI will make these assets available to the region through the following task force of eminent scientists, clinicians, educators and public health leaders.</p>             
             <div id="contributors">
              <div class="grid_9 primary alpha">
                <div class="alpha grid_3 alpha">
                	<small><a href="page-taskforce-landis.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_landis.jpg" /></a></p>
                </div>
                <div class="omega grid_5 alpha">
                <h4>Prof. Clive Landis</h4>
                    <p><strong>Chair</strong><br>
						Medical Researcher and Deputy Principal,<br>
UWI Cave Hill, <br>
Barbados</p>
<a href="page-taskforce-landis.php" class="fancybox-iframe">Click to view bio</a>
                	
                </div>
                 <div style="margin-top:20px;margin-bottom:20px;text-align:center;float:left;width:100%;">
                        <iframe style="width:100%;min-height:390px !important;height:auto;" src="https://www.youtube.com/embed/eX4Jvi8GLCY" frameborder="0" allowfullscreen></iframe>
                   </div>
             </div>
             
                <div class="grid_9 alpha omega">
                	<div class="grid_4 alpha">
                    	<div class="box-grey">
                        	<a href="page-taskforce-rose.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_rose.jpg" /></a></div>
                         <h4>Ms. Angela Mc Rose</h4>
                        <p>Chronic Disease Research Centre, <br>
UWI Cave Hill, <br>
Barbados - Infectious Disease Epidemiologist and Member of the Global Outbreak Alert &amp; Response Network (GOARN), WHO</p>
<small><a href="page-taskforce-rose.php" class="fancybox-iframe">Click to view bio</a></small><br>
                    </div>
                    <div class="grid_4 push_1 alpha omega">
                    	<div class="box-grey">
                        	<a href="page-taskforce-moore.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_moore.jpg" /></a>
                        </div>
                        <h4>Dr. Winston Moore</h4>
                        <p>Department of Economics, UWI Cave Hill<br>
Economist with expertise in Environmental Impact Assessments on Caribbean Economies</p>
<small><a href="page-taskforce-moore.php" class="fancybox-iframe">Click to view bio</a></small><br>
                    </div>
                 </div>
                
                  <div class="grid_9 alpha omega">
                	<div class="grid_4 alpha">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-chadee.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_chadee.jpg" /></a>
                    	</div>
                         <h4>Prof. Dave Chadee</h4>
                        <p>Department of Life Science, <br>
UWI St. Augustine, <br>
Trinidad - Environmental Health Researcher (a.k.a. 'The Mosquito Man')</p>
<small><a href="page-taskforce-chadee.php" class="fancybox-iframe">Click to view bio</a></small><br>
                 	</div>
                    <div class="grid_4 push_1 alpha omega">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-teelucksingh.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_teelucksingh.jpg" /></a>
                       </div> 
                       <h4>Prof. Surujpal Teelucksingh</h4>
                        <p>Faculty of Medical Sciences, <br>
UWI St. Augustine, <br>
Trinidad - Medical Researcher</p>
<small><a href="page-taskforce-teelucksingh.php" class="fancybox-iframe">Click to view bio</a> </p>
                    </div>
                 </div>
                
                 <div class="grid_9 alpha omega">
                	<div class="grid_4 alpha">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-lindo.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_lindo.jpg" /></a>
                        </div>
                        <h4>Prof. John Lindo</h4>
                        <p>Department of Microbiology<br>
UWI Mona, <br>
Jamaica – Medical Researcher </p>
<small><a href="page-taskforce-lindo.php" class="fancybox-iframe">Click to view bio</a></small><br>
                    </div>
                    <div class="grid_4 push_1 alpha omega">
                    	<div class="box-grey">
                    	 
                    	<img src="images/inside/taskforce/pic_strachan.jpg" />
                    </div>
                    <h4>Prof. Celia Christie-Samuels</h4>
                        <p>UWI Mona,<br>
Jamaica - Professor of Paediatrics (Infectious Diseases, Epidemiology and Public Health) </p>
                 </div>
                </div>
                
                 <div class="grid_9 alpha omega">
                	<div class="grid_4 alpha">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-howe.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_howe.jpg" /></a>
                    	</div>
                        <h4>Dr. Glenford Howe</h4>
                        <p>UWI Open Campus<br>
Social and Educational Research and Policy Analysis</p>
<small><a href="page-taskforce-howe.php" class="fancybox-iframe">Click to view bio</a></small><br>
					</div>
                    <div class="grid_4 push_1 alpha omega">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-simon.php" class="fancybox-iframe">
                    	<img src="images/inside/taskforce/pic_simon.jpg" /></a></div>
                        <h4>Dr. Veronica Simon</h4>
                        <p>Head<br>
UWI Open Campus,<br>
St Lucia <br>
Continuing Education &amp; Outreach</p>
<small><a href="page-taskforce-simon.php" class="fancybox-iframe">Click to view bio</a></small><br>
                    </div>
                 </div>
                  
                
                 <div class="grid_9 alpha omega">
                	<div class="grid_4 alpha">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-webber.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_webber.jpg" /></a>
                        </div>
                        <h4>Prof. Dale Webber</h4>
                        <p>UWI Centre,<br>
Jamaica<br>
Pro Vice Chancellor Graduate Studies &amp; Research</p>
<small><a href="page-taskforce-webber.php" class="fancybox-iframe">Click to view bio</a></small><br>
                    </div>
                    <div class="grid_4 push_1 alpha omega">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-gill.php" class="fancybox-iframe">
                    	<img src="images/inside/taskforce/pic_deforugill.jpg" /></a></div>
                        <h4>Dr. Dawn Marie De Four-Gill</h4>
                        <p>St. Augustine Campus, Trinidad<br>
University &amp; Campus Director, Marketing &amp; Communications</p>
<small><a href="page-taskforce-gill.php" class="fancybox-iframe">Click to view bio</a></small><br>
                    </div>
                 </div>
                  
                
                 <div class="grid_9 alpha omega">
                	<div class="grid_4 alpha">
                    	<div class="box-grey">
                    	 <a href="page-taskforce-vangorp.php" class="fancybox-iframe"><img src="images/inside/taskforce/pic_vangorp.jpg" />
                    </div>
                    <h4>Prof. Eric van Gorp</h4>
                        <p>Department of Viroscience, Erasmus University, Holland<br>
Virology and Technical Support</p>
<small><a href="page-taskforce-vangorp.php" class="fancybox-iframe">Click to view bio</a></small><br>
                 </div>
                  <div class="grid_4 push_1 alpha omega">
                   </div>
                    
                </div>
                
         </div>
         
             </div> <!-- end #copy -->
                
             </div>
        </div>
        
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   