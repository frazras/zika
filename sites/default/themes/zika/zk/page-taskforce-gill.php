<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dr. Dawn Marie De Four-Gill</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Dr. Dawn Marie De Four-Gill</h3>
            <p>Dr Dawn-Marie De Four-Gill is currently the University &amp; Campus Director of Marketing &amp; Communications at The University of the West Indies (UWI). A true Caribbean advocate she sees The UWI as one of the last, true representations of Caribbean unity and development, and believes that the institution has a key role in shaping the collective response to critical issues facing the region. She has therefore supported The UWI’s Open Campus Task Force and is currently serves on the One University Task Force and the recently convened regional Zika Task Force, all teams with a regional remit.</p>   
 
<p>As University Director, Marketing &amp; Communications, Dr Gill engages both external stakeholders and those across The UWI in developing strategic approaches to branding which highlight the relevance of the institution to regional development and support the institution’s international fund-raising drive.</p>
<p>Dr Gill is the Creative Director and Editor in Chief of The UWI’s Pelican Magazine and under her leadership, the Marketing &amp; Communications department has captured a number of prestigious international awards for marketing and communications publications in Higher Education from the Association of Commonwealth Universities, IABC and APEX. She is herself an 'academic-practioner', a Chevening Scholar and the holder of a Doctor of Education (Higher Education Management) from the University of Pennsylvania.
Dr Gill has developed specialist skills and acquired advanced training in: Enterprise and Business Development; Higher Education and Services Marketing; Project Management &amp; Evaluation; Corporate Communications and Negotiation Strategy. She also holds membership in numerous local, regional and international professional bodies and was the first Vice President (Communications) of the Trinidad &amp; Tobago Chapter of the International Association of Business Communicators (IABC).

</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>