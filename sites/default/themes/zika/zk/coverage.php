<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Media Centre |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />


 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside coverage">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/coverage/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Media Centre</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	<div class="grid_12">
             <div class="grid_3 omega alpha" id="sidebar">
             	<ul>
                	<li><a href="coverage.php">News</a></li>
                    <li><a href="events.php">Events</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="news"></a>
                       <h3>News</h3>
                       <div class="grid_3 alpha">
                       		<a href="http://www.nejm.org/doi/full/10.1056/NEJMsr1604338?query=featured_home" target="_blank"><h4>Zika Virus and Birth Defects — Reviewing the Evidence for Causality</h4></a><small>13 Apr 2016 | Source: NEJM</small></div>
                       <div class="grid_3 alpha">
                       		<a href="http://www.cdc.gov/media/releases/2016/s0413-zika-microcephaly.html" target="_blank"><h4>CDC Concludes Zika Causes Microcephaly and Other Birth Defects</h4></a><small>13 Apr 2016 | Source: CDC</small></div>
                       <div class="grid_3 alpha">
                       		<a href="http://today.caricom.org/2016/03/24/carpha-provides-zika-virus-update-in-caribbean/" target="_blank"><h4>CARPHA provides Zika Virus update in Caribbean</h4></a><small>24 Mar 2016 | Source: CARICOM</small></div>
                        <div class="grid_3 alpha">
                             <a href="http://nationwideradiojm.com/uwi-going-stepping-up-zika-fight/" target="_blank"><h4>Zika; a local and emotional issue</h4></a><small>10 Mar 2016 | Source: Nationwide News</small></div>
                        <div class="grid_3 alpha">
                       		<a href="https://www.facebook.com/media/set/?set=a.1005972202815613.1073741889.102903859789123&type=3" target="_blank"><h4>The UWI Responds to Zika - Press Conference</h4></a><small>10 Mar 2016</small>
                       </div>
                       <div class="grid_3 alpha">
                       		<a href="updates/update_09032016.php"><h4>UWI Task Force and Caribbean Experts Meet to Discuss Zika</h4></a><small>9 Mar 2016</small>
                       </div> 
                       <div class="grid_3 alpha">
                       		<a href="http://www.who.int/mediacentre/news/notes/2016/research-development-zika/en/" target="_blank"><h4>WHO and experts prioritize vaccines, diagnostics and innovative vector control tools for Zika R &amp; D</h4></a><small>9 Mar 2016  |  Source: WHO</small>
                       </div>
                       <div class="grid_3 alpha">
                       		<a href="http://www.thelancet.com/journals/lancet/article/PIIS0140-6736%2816%2900562-6/fulltext" target="_blank"><h4>Guillain-Barré Syndrome outbreak associated with Zika virus infection in French Polynesia: a case-control study</h4></a><small>29 Feb 2016  |  Source: Lancet</small>
                       </div>
                       <div class="grid_3 alpha">
                       		<a href="http://www.who.int/entity/emergencies/zika-virus/situation-report-26-02-2016.pdf?ua=1" target="_blank"><h4>Zika virus, Microcephaly, and Guillain–Barré syndrome situation report</h4></a><small>26 Feb 2016  |  Source: WHO</small>
                       </div>
                       <div class="grid_3 alpha">
                       		<a href="http://www.who.int/entity/emergencies/zika-virus/situation-report/19-february-2016/en/index.html" target="_blank"><h4>Zika, Microcephaly, and Guillain–Barré syndrome situation report</h4></a><small>19 Feb 2016  |  Source: WHO</small>
                       </div>
                       <div class="grid_3 alpha">
                       		<a href="http://www.who.int/entity/emergencies/zika-virus/situation-report/12-february-2016/en/index.html" target="_blank"><h4>Situation report: Zika and potential complications</h4></a><small>12 Feb 2016  |  Source: WHO</small>
                       </div>     
                
          </div>      
        </div>
         
        </div>
        
</div>
  
 </div> 
             
</div>   
      
  <?php include("incl-footer.php"); ?>   