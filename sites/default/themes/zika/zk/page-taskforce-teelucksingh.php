<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Prof. Surujpal Teelucksingh</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Prof. Surujpal Teelucksingh</h3>
            <p>Professor Surujpal Teelucksingh graduated with Honours in Medicine from The University of the West Indies in 1982, obtained his MRCP (UK) in 1988, a PhD from Edinburgh, Scotland in 1995 and an MBA in 1998 from The University of the West Indies.</p> 
<p>Professor Teelucksingh’s research interest lies in Metabolic Medicine which spans the full spectrum from basic science to clinical as well as public health medicine. His basic science research focuses on the interaction between diabetes and cardiac disease in the metabolic syndrome. In addition, he leads research is in the identification and treatment of diabetes in children and the early identification and treatment of diabetes in pregnancy. </p>

<p>He was Project Lead in the execution of an Inter American Development Bank Regional Public Good Project that unites the Caribbean in its battle against a common enemy- chronic non-communicable diseases- through the development and sharing of common methodologies for intervention, data capture and warehousing. He has also chaired a Cabinet-appointed Technical Advisory Committee that reported to the Minister of Health of the Republic of Trinidad and Tobago (2008-2011) on strategies to combat chronic diseases.

 
</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>