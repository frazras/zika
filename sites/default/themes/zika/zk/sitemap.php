<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Sitemap | The Task Force</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />

 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside generic">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Sitemap</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_2 alpha" id="sidebar">
             	<ul style="visibility:hidden;">
                	<li><a href="#news">News</a></li>
                    <li><a href="#events">Events</a></li>
                </ul>
             </div>
             
             <div class="grid_9 push_1 omega" id="copy"><a name="news"></a>
                 <p>This section will be populated soon.</p>             
                
          </div>      
             </div>
        </div>
        
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   