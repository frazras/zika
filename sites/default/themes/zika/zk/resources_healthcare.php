<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Resources for Public Health &amp; Health Care Professionals |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />

 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside resources">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Resources for You</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_12"><div class="grid_3 alpha" id="sidebar">
             	<ul>
                	<li><a href="resources_healthcare.php">For Public Health &amp; Health Care Professionals</a></li>
                    <li><a href="resources_policymakers.php">For Policy Makers</a></li>
                    <li><a href="resources_public.php">For the General Public</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="healthcare"></a>
                 <h3>For Public Health &amp; Health Care Professionals</h3>
                    <blockquote style="pusher">Public health surveillance is a key strategy in the fight against any disease [as is] a high index of suspicion by health care providers…It is important that we follow the course of this disease [Zika] in the Caribbean …We must provide the best epidemiological data in order for countries to keep abreast of the best practices in addressing the clinical manifestations ...</blockquote>          
              <p class="blockquote-author">Dr. Joy St. John, Director of Surveillance, Disease Prevention and Control, CARPHA</p>
              <h4>Microcephaly and Zika - WHO</h4>
              	<div class="row">
              		<iframe width="760" height="430" src="https://www.youtube.com/embed/0skonVosTJU" frameborder="0" allowfullscreen></iframe>
                </div>
              
               <h4>What should you know about Zika virus Testing? </h4>
               <ul>
               		<li><a href="http://www.carpha.org" target="_blank">CARPHA</a> has developed the capacity for ZIKV testing and is receiving samples from across the region.</li>
               		<li>Presently, Polymerase Chain Reaction (PCR) testing is the only reliable test for the Zika virus.</li>
               		<li>Sample testing is limited to those collected within the first 3 days of the illness.</li>
              		<li>Laboratory confirmation of Zika is specifically geared toward providing updates on the spread of the virus and is not aimed at identifying every single occurrence in a country.</li>
              		<li>Particular attention should be paid in the follow-up with pregnant patients who present febrile illness compatible with Zika and their off spring. </li>
               </ul>
               
               <h4>Useful Links &amp; Resources from our Partners</h4>
               <ul>
               <li><a href="http://www.nejm.org/doi/full/10.1056/NEJMsr1604338?query=featured_home" target="_blank">Zika Virus and Birth Defects — Reviewing the Evidence for Causality</a> | NEJM</li>
               <li><a href="http://www.cdc.gov/mmwr/volumes/65/wr/mm6511e3.htm" target="_blank">Preventing Transmission of Zika Virus in Labor and Delivery Settings Through Implementation of Standard Precautions — United States, 2016</a> | CDC</li>
               <li><a href="http://www.cdc.gov/mmwr/volumes/65/wr/mm6511e3.htm" target="_blank">Preventing Transmission of Zika Virus in Labor and Delivery Settings Through Implementation of Standard Precautions — United States, 2016</a> | CDC</li>
               	<li><a href="http://carpha.org/Portals/0/docs/ZIKA/Zika Virus_Surveillance.pdf" target="_blank">Zika: Strengthening the Health Sector Response</a> | CARPHA</li>
               	<li><a href="http://www.paho.org/hq/index.php?option=com_content&view=article&id=11601%3Azika-resources-for-health-authorities&catid=8424%3Acontent&Itemid=41694&lang=en" target="_blank">Zika Resources for Health Authorities</a> | PAHO</li>
               	<li><a href="http://www.paho.org/hq/index.php?option=com_content&view=article&id=10898&Itemid=41443&lang=en" target="_blank">Zika Epidemiological Alerts and Update</a>s | PAHO</li>
               	<li><a href="http://www.who.int/features/qa/zika-pregnancy/en/" target="_blank">The Zika Virus, Pregnancy and Microcephaly</a> | WHO </li>
               	<li><a href="http://www.who.int/mediacentre/factsheets/zika/en/" target="_blank">Zika Virus Fact Sheet</a> | WHO</li>
               	<li><a href="http://www.cdc.gov/zika/hc-providers/qa-pregnant-women.html" target="_blank">Caring for Pregnant Women possibly exposed to Zika virus</a> | CDC</li>
               	<li><a href="http://www.paho.org/hq/index.php?option=com_topics&view=article&id=333&Itemid=40921&lang=en" target="_blank">Zika: Risk and Outbreak Communications</a> | PAHO</li>
               
               </ul>
               
               <h4>Useful Links &amp; Resources from The UWI</h4>
               <ul>
               	<li><a href="docs/7H1N1ZikaPediatricIssuesFrederick.pdf" target="_blank">Pediatric Population Issues: H1N1 &amp; Zika</a></li>
                <li><a href="docs/8H1N1ZikaPregnancyRamjohn.pdf">Why Pregnancy is High Risk for H1N1 &amp; Zika</a></li>
               </ul>
               
            
              </div> 
               
          </div>      
             </div>
        </div>
        
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   