<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Prof. Clive Landis</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Prof. Clive Landis</h3>
            <p>Deputy Principal of The University of the West Indies’ Cave Hill campus, Clive Landis is Professor of Cardiovascular Research and a former Director of the Chronic Disease Research Center (CDRC). Professor Landis also founded the Edmund Cohen Laboratory for Vascular Research as an annex to the CDRC and the Immunology PhD programme at the Cave Hill campus. At the point of its 10th Anniversary, in 2013, the Edmund Cohen Laboratory had established itself as the leading vascular research laboratory in the Caribbean with over 50 peer reviewed scientific publications. </p> 
<p>Internationally, Professor Landis has served as The British Heart Foundation Lecturer in Cardiovascular Medicine at the Imperial College, London and has delivered distinguished lectures at conferences in the field of inflammation in the USA, UK, Europe, Australia, and New Zealand. </p> 
<p>Professor Landis’ research has been recognized with awards, including the Marvin Levin MD Award, by the American Diabetes Association, in Washington DC, 2006, The Principal’s Award for Excellence in 2009, and The Vice Chancellor’s Award for Excellence in 2010 from The University of the West Indies. </p>  
<p>In addition to the recent Chairmanship of the regional Zika Task Force, Professor Landis has taken on numerous national and regional leadership roles including:
</p> 
            <ul>
            	<li>Honorary Director of the Barbados Government HIV Laboratory</li>
            	<li>Chairman of the National HIV/AIDS Commission Research Committee</li>
            	<li>Founder and Chairman of the Caribbean Cytometry &amp; Analytical Society (CCAS)</li>
            	<li>Associate Editor Clinical Cytometry, Wiley.</li>
            </ul>
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>