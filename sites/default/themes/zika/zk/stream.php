<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Livestream |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />

 
 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>




<body class="inside generic">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Livestream</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_12"><div class="grid_3 alpha" id="sidebar">
             	<ul style="visibility:hidden;">
                	<li><a href="#news">News</a></li>
                    <li><a href="#events">Events</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="news"></a>
                  <h4>Live Stream: Zika Symposium</h4>
<p><strong>One Environment, One Health: Informing the Caribbean Response to Zika</strong><br>
Friday 4th March 2016, 9:30 A.M. - 1:00 P.M. AST</p>

<div class="streaming">
<script src="//content.jwplatform.com/players/L1cHPMm3-qMLxFQBv.js"></script>
</div>
        
          <div class="chatting">
           <script id="cid0020000092088348109" data-cfasync="false" async src="//st.chatango.com/js/gz/emb.js" style="width:680px;height: 400px;">{"handle":"cavehill-wide","arch":"js","styles":{"a":"48ab4d","b":100,"c":"000000","d":"000000","k":"48ab4d","l":"48ab4d","m":"48ab4d","p":"10","q":"48ab4d","r":100,"surl":0,"cnrs":"0.35","fwtickm":1}}</script>

</script>            
                
          </div>      
             </div>
        </div>
        
</div>
  
 </div>
      
  <?php include("incl-footer.php"); ?>   