     
    <div id="footer" class="container_12">
        <div class="mainfooter">
            <div id="footer-content" class="container_12">
                <div class="grid_6 alpha">
                    Copyright &copy; <?php echo date('Y'); ?> The University of the West Indies
                </div>
                <div class="grid_6 omega">
                    <ul>
                        <li><a href="contactus.php">Contact Us</a></li>
                        <li><a href="privacy.php">Privacy Statement</a></li>
                    </ul>
                </div>
            </div>
         </div>
    </div>  


</div>
</div>
 
<div id="jumper">
<a href="#top"><img src="/sites/default/themes/zika/zk/images/bn_backtotop.png" /></a>
</div> 




<style type="text/css">
@media screen and (min-width: 931px) {
	
	#header #logo.push_1 {
	padding-left:0;	
	}
	
	#header #mainnav.push_2 {
	left:0;	
	padding-left: 120px;
	}
	
	.home #midarea #news-featured {
    padding-top: 1em;
}
	
}
</style>

<?php include("incl-mobilenav.php"); ?> 

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">



<script src="http://uwi.edu/zika/js/modernizr-1.7.min.js" type="text/javascript"></script>

<!-- Responsive Tabs -->
<link rel="stylesheet" href="http://uwi.edu/zika/css/responsive-tabs.css" type="text/css">
<script src="http://uwi.edu/zika/js/responsiveTabs.js" type="text/javascript"></script>
<script>
		var jq89 = jQuery.noConflict();
jq89(document).ready(function($){
			RESPONSIVEUI.responsiveTabs();
		})
		</script>




<!-- CSS-driven animation -->
 <link rel="stylesheet" href="http://uwi.edu/zika/css/animate.css">
 
<script src="http://uwi.edu/zika/js/wow.js" type="text/javascript"></script>
		<script type="text/javascript">
              new WOW().init();
              </script>


<!-- Responsive menu -->      
 <link rel="stylesheet" href="http://uwi.edu/zika/css/slidebars.css">
<script src="http://uwi.edu/zika/js/slidebars.js" type="text/javascript"></script>
<script type="text/javascript">
var jq93 = jQuery.noConflict();
jq93(document).ready(function($){
					$.slidebars();
			}) ;
		</script>
        


 <!-- Fancybox -->    
<script type="text/javascript" src="http://uwi.edu/zika/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="http://uwi.edu/zika/js/helper-plugins/jquery.fancybox-media.js"></script>
<link href="http://uwi.edu/zika/js/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" /> 
            
<script type="text/javascript">
var jq94 = jQuery.noConflict();
jq94(document).ready(function($) {
  	$('.fancybox-media').fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		autoCenter :true,
		closeClick	: true,
		closeBtn:true,
		openEffect  : 'none',
		closeEffect : 'none',
		padding:0,
		helpers : {
			media : {}
		}
	});
  });
</script> 
<script type="text/javascript">
var jq95 = jQuery.noConflict();
jq95(document).ready(function($) {
  	$('.fancybox-iframe').fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '80%',
		height		: '80%',
		autoSize	: false,
		autoCenter :true,
		closeClick	: true,
		closeBtn:true,
		type : 'iframe',
		openEffect  : 'none',
		closeEffect : 'none',
		padding:0,
		helpers : {
			media : {}
		}
	});
  });
</script>




<!-- Smooth scrolling for anchor tags -->           
 <script type="text/javascript">
  var jq96 = jQuery.noConflict();
jq96(document).ready(function($){
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script> 




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74681499-1', 'auto');
  ga('send', 'pageview');

</script>


<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56d461365e4429be"></script>


</body>
</html>      
