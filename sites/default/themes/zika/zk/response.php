<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>The UWI's Response | The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />

<link rel="stylesheet" href="css/wide.css" media="screen and (max-width:959px) and (min-width: 768px)"/>
<link rel="stylesheet" href="css/medium.css" media="screen and (max-width:767px) and (min-width: 480px)"/>
<link rel="stylesheet" href="css/narrow.css" media="screen and (max-width:479px) and (min-width: 320px)"/> 
 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside response">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/response/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>The UWI's Response</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_12"><div class="grid_3 alpha omega" id="sidebar">
             	<ul>
                	<li><a href="#context">Context</a></li>
                    <li><a href="#contributions">Contributions</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="context"></a>
       	      <h3>Context</h3>
                
                <div class="grid_5 alpha">
                	<p>The University of the West Indies’ (UWI) regional <a href="taskforce.php">Zika Virus Task Force</a> will coordinate its response to the virus threat. The University has always owned its responsibility to provide the region with expert advice and consultative support; in keeping with this, the Task Force will consult on the development of a strategic, scientific approach to tackling the Zika virus. </p>
                </div>
               <div class="grid_4 omega" style="padding-top:40px;">
               		<img src="images/inside/response/pic_response.jpg" />
               </div>
                   <div style="margin-top:20px;text-align:center;float:left;width:100%;">
                        <iframe style="width:100%;min-height:390px !important;height:auto;" src="https://www.youtube.com/embed/iFtl_yMONmQ" frameborder="0" allowfullscreen></iframe>
                   </div>
               <hr />
                <a name="contributions"></a>
                <h3>Contributions</h3>
                <p>Some of the Task Force’s key contributions will include:</p>
                <ol>
                	<li>Helping to inform an aggressive and scientifically based prevention strategy to eliminate breeding sites for Aedes aegypti. </li>
                    <li>Working closely with the Caribbean Public Health Agency (CARPHA) and government ministries to pool resources to research and analyze the Zika outbreak and associated health complications in the Caribbean.</li>
                    <li>Conducting economic impact studies and engaging with ministries on outbreak preparedness. </li>
                    <li>Creating a regional observatory that will collate, organize and disseminate information about the virus and will make the university’s expertise accessible to researchers, government agencies, schools, health facilities and members of the general public.</li>
                    <li>Convening a major symposium involving regional ministries of health, donor agencies, national and regional public health agencies and tourism stakeholders.</li>
                    <li>Developing and implementing a comprehensive communications strategy for ensuring that information about the virus and its effects are shared broadly with the Caribbean public via radio, television, newspapers, the web and social media platforms.</li>
                    <li>Seeking grants to help mobilise resources for the region.</li>
                
                </ol>
                
       </div>         
                
                
             </div>
        </div>
        
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   