<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>UWI Task Force and Caribbean Experts Meet to Discuss Zika |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="../style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/1044_12_10_10.css" type="text/css" media="screen" />

 
 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">


<script src="../js/jquery-1.8.2.min.js"></script>
<script src="../js/modernizr-1.7.min.js"></script>


<script src="../js/smooth-scroll.js"></script>
<script src="../js/responsiveImg.js"></script>

<!-- bxSlider -->
<script type="text/javascript" src="../js/jquery.bxslider.js"></script>

<link href="../js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:true,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 815,
    slideMargin: 20,
    pause: 7000,
    auto:  true,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside generic">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
     <div class="sb-toggle-left" id="mobiletoggle">
           	<div class="navlines">
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            	<div class="navicon-line"></div>
            </div>
            <div class="navname">Menu</div>
     </div>  
     
     <div id="header">
            <div id="headercontent" class="container_12">
            	<div id="logo" class="push_1 grid_4 alpha">
                	<a href="../index.php"><img src="../images/logo.png" alt="UWI Responds to Zika"/></a>
                </div>
                <div id="mainnav" class="grid_6 push_2 omega">
                	<ul class="nav left grid_3 alpha">
                    	<li id="first"><a href="../index.php">Home</a></li>
                        <li id="second"><a href="../response.php">The UWI's Response</a></li>
                        <li id="third"><a href="../taskforce.php">The Task Force</a></li>
                        <li id="fourth"><a href="../situation.php">Situation Report</a></li>
                    </ul>
                    <ul class="nav right grid_3 omega">
                    	 <li id="fifth" style="display:none;"><a href="coverage.php">Featured Coverage</a></li>
                         <li id="sixth"><a href="../qa.php">Zika Q &amp; A</a></li>
                         <li id="seventh"><a href="../resources.php">Resources for You</a></li>
                         <li id="eighth"><a href="../contactus.php">Contact Us</a></li>
                    </ul>
                </div>
           </div>
      </div>
       
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_10 omega push_2" id="mainpic">
               		<img src="../images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Updates</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
       	    <div class="grid_2 alpha" id="sidebar">
             	<ul style="visibility:hidden;">
                	<li><a href="#news">News</a></li>
                    <li><a href="#events">Events</a></li>
                </ul>
             </div>
             
             <div class="grid_9 push_1 omega" id="copy"><a name="news"></a>
                  <h3>UWI Task Force and Caribbean Experts Meet to Discuss Zika</h3>
<p><strong>Regional Headquarters, Jamaica. March 9, 2016:</strong> Mapping an integrated Caribbean response to serious regional threats, such as the Zika virus disease, was the focus of a group of global experts at a symposium organised by the Vice-Chancellor of The University of the West Indies (The UWI).</p>

<p>About 70 delegates, including a newly commissioned UWI Regional Task Force on Zika, met in Bridgetown, Barbados on May 4-5, 2016 to hammer out new ways to harmonise the Caribbean’s response to the Zika virus and other public health emergencies. </p>

<p>The symposium brought together a diverse range of stakeholders, including government ministers, top regional academics and researchers from various disciplines, high-level public sector officials, leading health care professionals, and regional inter-governmental agency representatives. </p>

<p>On February 10, just days after the World Health Organisation (WHO) declared Zika a “public health emergency of international significance”, the Task Force was appointed by The UWI Vice-Chancellor Sir Hilary Beckles to harness the university’s extensive knowledge base and multidisciplinary research capacity, as powerful tools in the fight against the growing threat.</p> 

			

<p>It is chaired by Deputy Principal of The UWI Cave Hill campus and experienced medical researcher, Prof Clive Landis.</p>

<p>"The point of this Task Force is not to just fight Zika," Landis reminded delegates on March 5, during the final session of the two-day symposium. </p>

<p>He explained that it was intended to consider Zika, not in isolation, but as one of several present and future high-level health threats confronting the region. An outbreak of the Chikungunya virus swept sharply through the Caribbean in 2014, and the 2009 variant of H1N1 virus, known as Swine Flu, is now endemic in the region. </p>

<p>Professor Landis noted the Task Force was part of the University’s effort to assist Caribbean nations to coalesce their separate national health emergency response plans into a more collaborative, regional, interdisciplinary and sustained approach. Among the Task Force’s immediate scope of works includes establishing a laboratory molecular testing for Zika at The UWI and various outreach activities. Aside from the Symposium, public awareness includes engaging the public to profile and eliminate mosquito breeding sites based on the latest research on Aedes behavior and adaptation as well as managing a communications hub (www.uwi.ed/zika) to provide updates and helpful resources on Zika in the Caribbean. Other more long term response initiatives involve conducting research on the health impact in the Caribbean, surveillance and reporting of the epidemic’s trends and cases; strengthening outbreak preparedness and conducting research on the social and economic impact analysis of the virus.</p>

<p>The creation of the Task Force is a step towards improved information-sharing among countries, which could play a major role in reducing duplicated effort and misallocated resources, he said.</p>

<p>The 12-member Task Force includes: Prof Winston Moore, Economist; Prof Surujpal Teelucksingh, Medical Researcher; Angela Mc Rose, Infectious Disease Epidemiologist; Prof Dave Chadee, Environmental Health Researcher; Prof John Lindo, Medical Researcher; Celia Christie-Samuels, Infectious Disease Pediatrician; Dr Glenford Howe, Social Policy Analyst; Dr Veronica Simon, Head of the UWI Open Campus, Saint Lucia; Prof Dale Webber, UWI Pro Vice-Chancellor for Graduate Studies and Research; Eric van Gorp, a virologist at the Erasmus Medical Centre, Holland; and Dr Dawn-Marie De Four Gill, The UWI Director of Marketing and Communications.</p>

<div class="row">
	<ul id="slidebx">
                <li><img src="update_09032016/ZikaSymposium01-w640.jpg" /><br><small>Day one of The UWI's Zika Symposium – One Environment, One Health was live streamed to facilitate wider regional participation.</small></li>
                <li><img src="update_09032016/ZikaSymposium02-w640.jpg" /><br><small>(l-r) Professor Clive Landis, Zika Task Force Chair and Professor Eudine Barriteau,Pro Vice-Chancellor &amp; Principal, The UWI, Cave Hill greet a symposium participant. 
</small></li>
                <li><img src="update_09032016/ZikaSymposium03-w640.jpg" /><br><small>(l-r) Professor Clive Landis, Zika Task Force Chair and Professor Dave Chadee chat before the symposium.  
                </small></li>
                <li><img src="update_09032016/ZikaSymposium04-w640.jpg" /><br><small>Campus Principal, Cave Hill Professor Eudine Barriteau presents members of The UWI'S Regional Zika Task Force to The Hon. John Boyce, Minister of Health, Barbados.
                </small></li>
                <li><img src="update_09032016/ZikaSymposium05-w640.jpg" /><br><small>(l-r) Cave Hill Campus Principal, Professor Eudine Barriteau, The Hon. John Boyce, Minister of Health Barbados and UWI Zika Task Force Member Professor Dale Webber converse at the One Environment, One Health symposium informing the region's response to Zika.
                </small></li>
                <li><img src="update_09032016/ZikaSymposium06-w640.jpg" /><br><small>Endorsing the partnership between regional governments and the regional institution, The Hon. John Boyce, Minister of Health, Barbados addresses the One Environment, One Health Symposium held on March 4-5. 
                </small></li>
                <li><img src="update_09032016/ZikaSymposium07-w640.jpg" /><br><small>Attentive symposium participants.
                </small></li>
                <li><img src="update_09032016/ZikaSymposium08-w640.jpg" /><br><small>UWI expert, Professor Christine Carrington presents on <em>Zika in the Americas</em>. 
                </small></li>
                <li><img src="update_09032016/ZikaSymposium09-w640.jpg" /><br><small>UWI expert, Professor Dave ‘The Mosquito Man’ Chadee facilitates a closed session on <em>Aedes Aegypti Biology and Control</em>. 
                </small></li>
                <li><img src="update_09032016/ZikaSymposium10-w640.jpg" /><br><small>UWI expert, Professor Dave ‘The Mosquito Man’ Chadee facilitates a closed session on <em>Aedes Aegypti Biology and Control</em>.
                </small></li>
                <li><img src="update_09032016/ZikaSymposium11-w640.jpg" /><br><small>Partnering the with the Regional Zika Task Force, Rachael Albalack, from the  Centre for Disease Control and Prevention (CDC) presents on<em> Zika in Pregnant Women</em>.
                </small></li>
                <li><img src="update_09032016/ZikaSymposium12-w640.jpg" /><br><small>Symposium participant engages the experts with a question. 
                </small></li>
              </ul>
              <br><a href="https://www.facebook.com/media/set/?set=a.1002509489828551.1073741888.102903859789123&type=3" target="_blank">View our full Symposium album on Facebook</a>
</div>

 <p style="text-align:center;">###</p>

<h5>About The UWI</h5>
<p>Since its inception in 1948, The University of the West Indies (UWI) has evolved from a fledgling college in Jamaica with 33 students to a fully-fledged, regional University with over 50,000 students. Today, The UWI is the largest, most longstanding higher education provider in the Commonwealth Caribbean, with three physical campuses in Barbados, Jamaica, Trinidad and Tobago, and an Open Campus. The UWI serves 17 English-speaking countries and territories in the Caribbean: Anguilla, Antigua and Barbuda, The Bahamas, Barbados, Belize, Bermuda, The British Virgin Islands, The Cayman Islands, Dominica, Grenada, Jamaica, Montserrat, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Trinidad and Tobago, and Turks and Caicos. The UWI’s faculty and students come from more than 40 countries and The University has collaborative links with 160 universities globally; it offers undergraduate and postgraduate degree options in Food and Agriculture, Engineering, Humanities and Education, Law, Medical Sciences, Science and Technology and Social Sciences. The UWI’s seven priority focal areas are linked closely to the priorities identified by CARICOM and take into account such over-arching areas of concern to the region as environmental issues, health and wellness, gender equity and the critical importance of innovation. </p>
<small>(Please note that the proper name of the university is The University of the West Indies, inclusive of the "The", hence The UWI.)</small>


         
                
          </div>      
             </div>
        </div>
        
</div>
  
 
      
  <?php include("../incl-footer.php"); ?>   