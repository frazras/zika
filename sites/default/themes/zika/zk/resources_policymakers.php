<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><meta name="Description" content="" />
<meta name="Keywords" content="" />

<title>Resources for Policy Makers |  The UWI Responds to Zika</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
 


<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS for IE
  ================================================== -->
	

<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if IE 9]>
	<link href="css/ie9.css" rel="stylesheet" type="text/css" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<!--[if IE 10]>
	<link href="css/ie10.css" rel="stylesheet" type="text/css" />
<![endif]--> 
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>	
<![endif]-->

<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/modernizr-1.7.min.js"></script>


<script src="js/smooth-scroll.js"></script>
<script src="js/responsiveImg.js"></script>



<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" type="text/css" media="screen" />
    
<script type="text/javascript">
var jq88 = jQuery.noConflict();
jq88(document).ready(function($){
  $('#slidebx').bxSlider({
    mode: 'horizontal',
    infiniteLoop: true,
	adaptiveHeight:false,
	adaptiveHeightSpeed:600,
    speed: 700,
	slideWidth: 460,
    slideMargin: 20,
    pause: 7000,
    auto:  false,
    pager: true,
    controls: false
  });
});
</script>



<body class="inside resources">

<div class="outer" id="sb-site">
	<div id="wrap">
    
    
    
      <?php include("incl-header.php"); ?>  
    
      
      <div id="insidecontent">
          
          
          <div id="toparea" class="container_12">
               <div class="grid_9 omega push_3" id="mainpic">
               		<img src="images/inside/pic_main.jpg" />
               </div> 
                <div class="grid_12 alpha" id="pagetitle"><a name="top"></a>
                    <h2>Resources for You</h2>
                </div>
            </div> 
        
        
        <div id="midarea" class="container_12">
        	 <div class="grid_12"><div class="grid_3 alpha" id="sidebar">
             	<ul>
                	<li><a href="resources_healthcare.php">For Public Health &amp; Health Care Professionals</a></li>
                    <li><a href="resources_policymakers.php">For Policy Makers</a></li>
                    <li><a href="resources_public.php">For the General Public</a></li>
                </ul>
             </div>
             
             <div class="grid_9 omega" id="copy"><a name="healthcare"></a>
               <h3>For Policy Makers</h3>
               <blockquote>
               		The Aedes aegypti is multiplying rapidly in our region. What is needed on our part is a similarly aggressive and collaborative approach, we must address the threat of Zika as a region. We’ve been here before with Malaria which we successfully eliminated from the Caribbean, and we can do it again... 
               </blockquote>
               <p class="blockquote-author">Prof. Clive Landis, Chair UWI Zika Task Force </p>
               <p>Effective public health policy is critical to support the sustained fight against the Zika virus and other epidemics that can potentially affect the small-island states of the Caribbean. Our region’s current vulnerability to vector borne viruses such as Zika virus, Dengue and Chikungunya demands that our response be based on solid scientific research and collaboration at the highest levels. The regional Zika Task Force commissioned by The University of the West Indies will support regional governments in the collaborative bid to develop the required public health policies in battling this common threat.</p>
               
               <h4>Useful Links &amp; Resources from our Partners</h4>
               <ul>
               	<li><a href="http://carpha.org/Portals/0/docs/ZIKA/Zika Virus_Tourism.pdf" target="_blank">Zika and Caribbean Tourism</a> | CARPHA
               	<li><a href="http://www.paho.org/hq/index.php?option=com_docman&task=doc_view&Itemid=270&gid=33051&lang=en" target="_blank">Zika: A step by step guide to Risk Communications and Community Engagement; 2016</a> | PAHO</li>
               	<li><a href="http://www.who.int/emergencies/zika-virus/response/en/" target="_blank">Zika: Strategic Response Framework and Joint Operations Plan (January – June 2016)</a> | WHO</li>
               	<li><a href="http://www.paho.org/hq/index.php?option=com_content&view=article&id=11647%3Astrategy-for-enhancing-national-capacity-to-respond-to-zika-virus-epidemic-in-the-americas&catid=8482%3A2016-zika-resource-mobilization&Itemid=41727&lang=en" target="_blank">Zika: Risk and Outbreak Communications</a> | PAHO</li>
               	<li><a href="http://www.paho.org/hq/index.php?option=com_content&view=article&id=11647%3Astrategy-for-enhancing-national-capacity-to-respond-to-zika-virus-epidemic-in-the-americas&catid=8482%3A2016-zika-resource-mobilization&Itemid=41727&lang=en" target="_blank">Strategy for Enhancing National Capacity to respond to Zika virus in the Americas</a> | PAHO</li>
               	<li><a href="https://cursos.campusvirtualsp.org/course/view.php?id=139" target="_blank">Self-Learning Course on Risk Communication</a> | PAHO/WHO</li>
               	<li><a href="http://www.paho.org/hq/index.php?option=com_content&view=category&layout=blog&id=8482&Itemid=41727&lang=en" target="_blank">Zika: Resource Mobilization</a> | PAHO</li>
               
               </ul>
        </div>       
              
               
          </div>      
             </div>
        </div>
        
</div>
  
  
             
</div>   
      
  <?php include("incl-footer.php"); ?>   