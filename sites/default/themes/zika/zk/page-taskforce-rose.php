<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ms. Angela Mc Rose</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Ms. Angela Mc Rose</h3>
            <p>Ms Angela Mc Rose is Head of the Non-communicable Disease (NCD) Surveillance team and Senior Epidemiology Lecturer at the Chronic Disease Research Centre, The University of the West Indies (UWI) Cave Hill. She brings to the regional Zika Task Force more than 20 years of international experience primarily in the area of infectious disease epidemiology. Over the years her work in the field has seen her contribute to initiatives in Europe and Africa including the UK, France, Finland, and most recently an international mission with the World Health Organisation (WHO) during its response to the Ebola outbreak in Guinea.</p> 
            <p>Ms Rose’s strong competence in disease surveillance systems has come from her work with some major international agencies. These include the Health Protection Agency’s Centre for Infections in the UK and NGO Médecins sans Frontières (Doctors without Borders), where her work covered infectious disease surveillance systems evaluation and operational research related to disaster and outbreak response. Ms Rose is currently pursuing her PhD in Epidemiology at The UWI, Cave Hill and is also a regular member of the abstract review team for the European Centre for Disease Control’s annual scientific conference on Applied Infectious Disease Epidemiology. </p> 
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>