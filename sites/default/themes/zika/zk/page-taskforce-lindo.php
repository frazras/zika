<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Prof. John Lindo</title>

<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1044_12_10_10.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen" />

<style>
body.blank {
 background:transparent; }
body.blank .grid_10 {
padding-top:2em;  
}

body.blank .grid_10 p,body.blank .grid_10 h3 {
padding-bottom:10px;  
}

body.blank .grid_10 p {
font-size:17px;	
}

.pdf {
  background: transparent url('images/icon_pdf.png') no-repeat scroll left 7px;
   padding: 20px 0 20px 40px;
   border: none;
}

body.blank ul li {
display:block;
  padding-bottom:0px;
  padding-left:30px;
    font-size: 18px;
    margin-top: 15px;
width: 85%;
background: transparent url(images/inside/bullet_yellow.png) top left no-repeat;

}

</style>

</head>

<body class="blank">
	<div class="container_12">
		<div class="grid_10">
            <h3>Prof. John Lindo</h3>
            <p>Professor Lindo John is a Full Professor of Parasite Epidemiology, Faculty of Medical Sciences at The University of the West Indies (UWI), Mona campus and Deputy Dean (Research). His extensive experience with Vector borne diseases comes in part from a sabbatical year spent with the Neglected Tropical Diseases Unit at the Pan American Health Organization (PAHO) in Washington DC.  During his time at PAHO he worked on the elimination of schistosomiasis in the Americas which included a visit to inspect transmission sites in Suriname and facilitating a workshop in Grenada with experts from academia, WHO, CARPHA and CDC. </p> 

<p>A Consultant Parasitologist to the University Hospital of the West Indies, Professor John also has responsibility for Jamaica’s only Virology laboratory where Dengue, Chikungunya, Zika and all other vector borne viruses are diagnosed. He is well published and has held the positions of Fogarty Fellow, University of Miami School of Medicine and visiting Assistant Professor at Nova Southeastern University, Florida. Professor John has also lent research support to the Jamaican Ministry of Health, The National Health Fund, the Chase Fund and PetroCaribe. </p>

<p>His contributions to science include extensive studies on the Emergence of Rat lungworm disease in Jamaica; Diagnosis and re-emergence of malaria with particular focus on Guyana Honduras and Jamaica; Epidemiology of Strongyloides stercoralis infections; and the clinical disease and epidemiology of free living amoebae in Jamaica and Spain. 

</p> 
            
            <p>&nbsp;</p>
       
	</div>
 </div>
</body>
</html>