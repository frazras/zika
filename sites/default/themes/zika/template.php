<?php 

function zika_preprocess_page(&$vars) {
    if (isset($vars['node']->type)) {
        $vars['theme_hook_suggestions'][]='page__'.$vars['node']->type;
    }
}

function zika_preprocess_html(&$vars) {
  $path = drupal_get_path_alias();
  $aliases = array_unique(array_merge(preg_split("/[\/]/", str_replace('-','',$path)), preg_split("/[\/\-]/", $path)));
  global $short, $body_classes;
  if ($short){
    $vars['classes_array'][] = 'short'.$body_classes;
  }

  $stop=false;
  foreach($aliases as $alias) {
    $vars['classes_array'][] = drupal_clean_css_identifier($alias);
    if(($alias!='home')&&!$stop){
      $vars['classes_array'][] = 'inside';
      $stop=true;
    }
  }
}

function zika_process_page(&$variables) {
    $menu_tree = menu_tree_all_data('main-menu');
    $variables['main_menu'] = menu_tree_output($menu_tree);
}

function zika_menu_tree($variables) {
  switch ($variables['theme_hook_original']) {
    case 'menu_tree__menu_mainlinks':
    case 'menu_tree__menu_menu_footer_nav2':
    case 'menu_tree__menu_menu_footer_nav1':
    case 'menu_tree__menu_menu_footer_webmain':
    case 'menu_tree__menu_menu_footer_contacts':
        return '<ul>' . $variables['tree'] . '</ul>';
        break;
    default:
        return '<ul class="menu">' . $variables['tree'] . '</ul>';
  }
  
}
function zika_menu_tree__main_menu($variables) {
  global $mainLinkCount;
  $mainLinkCount=0;
    return '<ul class="menu nav left grid_3 alpha">' . $variables['tree'] . '</ul>';
}

function zika_menu_link__main_menu(array $variables) {
  global $mainLinkCount;
  $element = $variables['element'];
  $sub_menu = '';
  $main_link_sep='';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  $mainLinkCount++;
  if($mainLinkCount==5)$main_link_sep='</ul><ul class="menu nav left grid_3 alpha">';
  return $main_link_sep.'<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function zika_page_alter(&$vars){
  
  if(current_path()=='node/34'){
    $vars['content']['quicktabs_programmes']['content']['tabs']['#prefix']='<div class="five columns alpha">';
    $vars['content']['quicktabs_programmes']['content']['tabs']['#suffix']='</div>';
    $vars['content']['quicktabs_programmes']['content']['container']['#prefix']=' <div class="eleven columns omega" id="instructions">'.$vars['content']['quicktabs_programmes']['content']['container']['#prefix'];
    $vars['content']['quicktabs_programmes']['content']['container']['#suffix'].='</div>';
    $vars['content']['custom_search_blocks_1']['custom_search_blocks_form_1']['#prefix'] = '<span id="programmesearchbox" class="ten columns">';
    $vars['content']['custom_search_blocks_1']['custom_search_blocks_form_1']['#suffix'] = '</span>';
    $vars['content']['custom_search_blocks_1']['actions']['#prefix'] = '<span id="programmesearchgo">';
    $vars['content']['custom_search_blocks_1']['actions']['#suffix'] = '</span><div class="four columns alpha box-orange" id="newcourses">
                        <h4>Featured Academic Programmes</h4>
                              <ul>
                                  <li><a href="http://www.open.uwi.edu/cpe/certificate-climate-change">Certificate in Climate Change</a></li>
                <li><a href="http://www.open.uwi.edu/undergraduate/bsc-social-work">BSc in Social Work</a></li>
                <li><a href="https://www.open.uwi.edu/undergraduate/diploma-social-work">Diploma in Social Work</a></li>
                                <li><a href="http://www.open.uwi.edu/undergraduate/certificate-social-work">Certificate in Social Work</a></li>
                                <li><a href="http://www.open.uwi.edu/graduate/ma-english-language">MA in English Language</a></li>
                                </ul>
                      </div>';
  }
}

function zika_field__field_availability__programme($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h5 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h5>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '| <a href="programmes">Search all Open Campus programmes</a></div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function zika_field__programme ($variables) {
  if ($variables['element']['#field_type']=='entityreference') {
    ddl($variables);
    $output = '';
    // Render the label, if it's not hidden.
    if (!$variables['label_hidden']) {
      $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</div>';
    }

    // Render the items.
    $output .= '<div class=" sixteen columns AccordionX field-items"' . $variables['content_attributes'] . '>';
    foreach ($variables['items'] as $delta => $item) {
      $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
      $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
    }
    $output .= '</div>';

    // Render the top-level DIV.
    $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

    return $output;
  }
}
